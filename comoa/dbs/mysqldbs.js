const mysql = require("mysql2/promise");
const config = require("./config");

const pool = mysql.createPool(config);
console.log('Connection pool created.!!!!');

pool.on('acquire', function (connection) {
  console.log(`Connection ${connection.threadId} acquired`);
});

pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

pool.on('release', function (connection) {
    console.log(`Connection ${connection.threadId} released`);
});

// const getConn =async function(callback) {
//     await pool.getConnection(async function(err, connection) {
//         if(!err) {
//             callback(conn);
//           }
//         else{
//             console.log(err);
//         }
//     });
//   }
  
//const connect1 = pool.getConnection(async conn => conn);
module.exports = pool;



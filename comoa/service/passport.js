// service/passport.js - 로그인 기능
const passport = require('passport'),
        LocalStrategy = require('passport-local').Strategy;

const bcrypt = require('bcrypt');
// const User = require('../mongoDB/schema/user');
//const user = require('../mongoDB/userMongo');
const db = require("../dbs/mysqldbs");
const axios = require('axios')
// Local 전략 세움
module.exports = () => {
    // 로그인 성공 시 호출
    // stategy 성공 시 호출
    passport.serializeUser((user, done) => {
        // user가 desertializeUser의 첫 번째 매개변수로 이동
        done(null, user);
    });

    // 로그인 성공 후 페이지 마다 정보 호출
    // 매개변수 user는 serializeUser의 done인자 user를 받은 것
    passport.deserializeUser((user, done) => {
        // 여기의 user가 req.user가 됨
        done(null, user);
    });

    // 로그인 인증
    passport.use(new LocalStrategy({
        usernameField: 'email',         // 이메일 property 설정
        passwordField: 'password',      // 패스워드 property 설정
        session: true,
        passReqToCallback: false,
    }, async (email, password, done) => {
        //let data = {email: email};
        //let userInfo = await user.findOne(data);

        var data = JSON.stringify({ username : email, password : password });
        // const options = {
        //     hostname: '10.153.1.25',
        //     port: 6800,
        //     path: '/ticSso//business/api.v1.sso/person/login',
        //     method: 'POST',
        //     headers: {
        //       'Content-Type': 'application/json',
        //     },
        //   };
        //   const req =  http.request(options, res => {
        //     console.log(`statusCode: ${res.statusCode}`)
          
        //     res.on('data', d => {
        //       process.stdout.write(d)
        //     })            
        //   })
        //   req.on('error', error => {
        //     console.error(error)
        //   })
        //   req.write(data);
        //   req.end();
        const headers = {
            'Content-Type': 'application/json',
            'pid' : 'pid.leads',
            'sign' : '7abb18eb068a631ea33267e9dd038253',
            'timestamp' : '1629923161994'
          };
          //http://10.153.1.25:6800/ticSso//business/api.v1.sso/person/login2
          //http://10.153.1.25:8509/business/api.v1.sso/person/login2
        let resultcode = 1;
        await axios.post('http://10.153.1.25:6800/ticSso//business/api.v1.sso/person/login2', {
            username : email, 
            password : password
        }).then(res => {
            console.log(`statusCode: ${res.status}`);
            console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            console.log(res.resultCode);
            console.log(res.data);
            resultcode = res.data.resultCode;
        }).catch(error => {
            console.error(error)
        })
        
        if(resultcode != '0'){
          //  return done(null, false, {message: '아이디 또는 패스워드를 확인해 주세요.'});
        }

        let con1 = await db.getConnection(async conn => conn);
        var [rows, fields] = await con1.query("select member_id, member_name, member_tel, member_mobile, team, po, level from member_info where member_id = ?", email);
        con1.release();
        var userInfo = JSON.parse(JSON.stringify(rows));
        
        //console.log(userInfo[0]);
        if(userInfo){
            return done(null, userInfo[0]);
        }
        else{
            //등록페이지 이동
            return done(null, false, {message: '등록된 사용자가 아닙니다.'});
        }

        // if(userInfo) {
        //     // Hash 암호화 패스워드 비교
        //     let bool = bcrypt.compareSync(password, userInfo.password);
        //     if(bool) {                  // 로그인 성공
        //         return done(null, userInfo);
        //     } else {                    // 패스워드 에러
        //         console.log('패스워드가 틀렸습니다.');
        //         return done(null, false, {message: '아이디 또는 패스워드를 확인해 주세요.'});
        //     }
        // } else {                        // 등록 되지 않은 사용자
        //     console.log('없는 사용자 입니다.');
        //     return done(null, false, {message: '아이디 또는 패스워드를 확인해 주세요.'});            
        // }
    }));    
}


const { render } = require("../app");
const { get } = require("../routes");
const boardMongo = require('../mongoDB/boardMongo');
const board = require("../mongoDB/schema/board");
const mongoose = require("mongoose");
const replyMongo = require("../mongoDB/replyMongo");
const db = require("../dbs/mysqldbs");

module.exports = {
    // 게시글 리스트(페이징)
    showList: async (req, res, next) => {
        // 게시글 타입 선별(자유, 코로나)
        let type = req.params.type;
        console.log(type);
        var resultCode = type =='wait' ? 1 : type =='receive' ? 2 : 3;
        console.log(resultCode);

        let con1 = await db.getConnection(async conn => conn);
        var [rows, fields] = await con1.query("select count(id) as B from ticket where result_code = ?", resultCode);
       // con1.release();
        //var resultArray = Object.values(JSON.parse(JSON.stringify(rows)))
        var resultArray = JSON.parse(JSON.stringify(rows));
        let count = resultArray[0].B;

        let start = false;
        let end = false;
        let nowPage = req.query.startPage;
        let lastPage = parseInt(count / 10);

        if(count % 10 != 0) lastPage += 1;

        // 페이징 버튼 활성화를 위한 변수 값 등록
        if(nowPage > 1) start = true;
        if(lastPage > 1) end = true;
        if(lastPage == nowPage) end = false;

        // 데이터 출력을 위한 JSON 형식
        let data = {
            type: type,
            sort: -1,
            maxPage: 10,
            startPage: (nowPage-1) * 10
        };

        
        var [rows, fields] = await con1.query("select id as _id,type as title, phone as name, gmt_create as reg_date   from ticket where result_code=? LIMIT ?,10", [resultCode,(nowPage-1)]);
        con1.release();
        var resultArray = Object.values(JSON.parse(JSON.stringify(rows)))
        let boardNum = count-((nowPage-1)*data.maxPage);

        res.render('boards', {type: data.type, list: resultArray, nowPage: nowPage, start: start, end: end, boardNum: boardNum, lastPage: lastPage});
    }, 

    // 게시글 내용 VIEW
    showBoard: async (req, res, next) => {        
        let type = req.params.type;                             // 게시글 타입
        let _id = req.params._id                                // 게시글 MongoDB _id

        console.log(_id);
        let con1 = await db.getConnection(async conn => conn);
        var [rows, fields] = await con1.query("select id as _id,type,company ,customer, content, phone, email, provice, company, gmt_create, service_name, trade_name " +
                                            "from ticket where id = ?",_id);
        con1.release();
        //var resultArray = Object.values(JSON.parse(JSON.stringify(rows)))
        var resultArray = Object.values(JSON.parse(JSON.stringify(rows)));
        let reply = [];           // 해당 게시글의 _id를 가지고 있는 댓글정보 가져오기
        
        res.render('showBoard', {type: type, board: resultArray[0], reply: reply});
    },

    // 게시글 등록
    registerBoard: (req, res, next) => {
        let type = req.params.type;                            // 게시글 타입

        if(req.method === 'GET') {                             // 요청 메소드 판별
            res.render('registerBoard', {type: type});         // GET일 경우 페이지 render
        } else {                                               // POST일 경우
            let body = req.body;
            let data = {                                       // 입력 데이터 값
                title: body.title,                
                content: body.content,
                type: type,
                reg_date: new Date,
                writer: {
                    email: req.user.email,
                    name: req.user.name
                }
            };

            boardMongo.registerBoard(data);                    // MongoDB에 저장
            res.redirect(`/boards/${type}?startPage=1`);
        }
    },

    // 게시글 수정
    modifyBoard: async (req, res, next) => {
        let _id = req.params._id;                                   // 해당 게시글 _id

        if(req.method === 'GET') {                                  // 요청 메소드 판별
            let board = await boardMongo.findBoard(_id);            // GET일 경우 해당 게시글 정보를 render페이지에 전달

            res.render('modifyBoard', {board: board});
        } else {                                                    // PUT일 경우
            let body = req.body;
            let query = {_id: mongoose.Types.ObjectId(body.id)};    // update 할 데이터
            let data = {                                            // update 수정 내용
                $set: {
                    title: body.title,
                    content: body.content
                }
            }
            boardMongo.updateBoard(query, data);                    // MongoDB update
            res.redirect(`/boards/${body.type}?startPage=1`);
        }
    },

    // 게시글 삭제
    deleteBoard: (req, res, next) => {
        let type = req.params.type;                                 // 페이지를 돌아가이 위한 타입 정보
        let id = req.body.id;                                       // 해당 게시글 _id
        let data = {_id: mongoose.Types.ObjectId(id)};              // MongoDB에 찾을 게시글 데이터
        let rData = {board_id: mongoose.Types.ObjectId(id)};        // MongoDB에 찾을 댓글 데이터

        boardMongo.deleteBoard(data);                               // 해당 _id 게시글 데이터 delete
        replyMongo.replyDelete(rData);                              // 해당 게시글 _id를 가지고 있는 댓글 데이터 delete
        
        res.redirect(`/boards/${type}?startPage=1`);
    }
};
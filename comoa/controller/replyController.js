const { SchemaTypes } = require('mongoose');
const replyMongo = require('../mongoDB/replyMongo');
const mongoose = require("mongoose");
const db = require("../dbs/mysqldbs");

module.exports = {
    // 해당 게시글 댓글 리스트
    listReply: async (req, res, next) => {
        let body = req.body;
        // 게시글 _id로 댓글 출력
       // let replies = await replyMongo.replyList({board_id: mongoose.Types.ObjectId(body.id)});
       let con1 = await db.getConnection(async conn => conn);
        var [rows, fields] = await con1.query("select reply_id, name, content, create_date, state from reply where ticket_id = ?", body.id);
        con1.release();
        var replies = Object.values(JSON.parse(JSON.stringify(rows)))
        
        res.json({replies: replies});
    },

    // 댓글 등록
    insertReply: async (req, res, next) => {
        console.log('!!!!!asdsd!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        let body = req.body;
        var state = body.state == 'option1' ? 2 :  body.state == 'option3' ? 3 : 0;
        console.log(body.state);
        
        let result = 'ok';
        let con1 = await db.getConnection(async conn => conn);
        console.log(state);
        if(state > 0){
            var [rows, fields] = await con1.query("update ticket set result_code = ? where id = ?", [state, body.id] );

            if(rows.affectedRows == 0){
                res.json({status: 'notFound'});   
            }
        }


        var r1= await con1.query("insert into reply (`ticket_id`, `name`, `content`, `state`) values (?,?,?,?)",[body.id, body.name,body.content, state] );
        con1.release();
        console.log(r1);

        if(result == 'ok') res.json({status: 'OK'});
        else res.json({status: 'NO'});        
    },

    // 댓글 삭제
    deleteReply: async (req, res, next) => {
        let body = req.body;
        let data = {
            _id: mongoose.Types.ObjectId(body.reply_id)
        }

        // 댓글 _id를 이용한 delete
        replyMongo.replyDelete(data);

        res.json({status: 'OK'}); 
    }
}